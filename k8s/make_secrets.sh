#
# Create Kubernetes secrets for AWS authentication 
# Or microk8s secrets based on environment variables
#
if [ ${HSDS_USERNAME} ] && [ ${HSDS_PASSWORD} ]; then
  echo -n ${HSDS_USERNAME} > /tmp/hsds_username
  echo -n ${HSDS_PASSWORD} > /tmp/hsds_password

  # Create the secret
  #kubectl --namespace hobo create secret generic hobo-user --from-file=/tmp/hsds_username --from-file=/tmp/hsds_password
  microk8s kubectl create secret generic hobo-user --from-file=/tmp/hsds_username --from-file=/tmp/hsds_password

  # Delete the temp files
  rm /tmp/hsds_username
  rm /tmp/hsds_password
fi

if [ ${HOBO_CLIENT_ID} ] && [ ${HOBO_CLIENT_SECRET} ]; then
  echo -n ${HOBO_CLIENT_ID} > /tmp/hobo_client_id
  echo -n ${HOBO_CLIENT_SECRET} > /tmp/hobo_client_secret

  # Create the secret
  #kubectl --namespace hobo create secret generic hobo-client --from-file=/tmp/hobo_client_id --from-file=/tmp/hobo_client_secret
  microk8s kubectl create secret generic hobo-client --from-file=/tmp/hobo_client_id --from-file=/tmp/hobo_client_secret

  # Delete the temp files
  rm /tmp/hobo_client_id
  rm /tmp/hobo_client_secret
fi

