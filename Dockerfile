# syntax=docker/dockerfile:1.2

FROM python:3.9-slim-bullseye

RUN apt update \
 && apt install -y git \
 && git clone https://gitlab.com/uva-arc/hobo-request.git hobo-request

WORKDIR /hobo-request
RUN pip install -r requirements.txt
COPY src src

ENTRYPOINT ["python", "-u", "src/hoborequest/hoboconnect.py"]

#RUN --mount=type=secret,id=hobo-conf,dst=/hobo-request/conf/hobo-connect.conf python src/hobo-connect.py
